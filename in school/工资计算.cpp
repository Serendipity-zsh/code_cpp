#include<iostream>
using namespace std;

int main(){
	int t;
	int s;
	cin>>t;
	if(t<=3500){
		cout<<t<<endl;
	}
	else if(t>3500&&t<=5000){
		s=(t-3500*0.03)/0.97;
		cout<<s<<endl;
	}
	else if(t>5000&&t<=8000){
		s=(t-5000*0.1+45)/0.9;
		cout<<s<<endl;
	}
	else if(t>8000&&t<=12500){
		s=(t-8000*0.2+45+300)/0.8;
		cout<<s<<endl;
	}
	else if(t>12500&&t<=38500){
		s=(t-12500*0.25+45+300+900)/0.75;
		cout<<s<<endl;
	}
	else if(t>38500&&t<=58500){
		s=(t-38500*0.3+45+300+900+6500)/0.7;
		cout<<s<<endl;
	}
	else if(t>58500&&t<=83500){
		s=(t-58500*0.35+45+300+900+6500+6000)/0.65;
		cout<<s<<endl;
	}
	else{
		s=(t-83500*0.45+45+300+900+6500+6000+8750)/0.55;
		cout<<s<<endl;
	} 
	return 0;
} 
