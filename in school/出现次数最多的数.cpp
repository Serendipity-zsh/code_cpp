#include<stdio.h>
#include<stdlib.h>
struct In{
	int a;
	int b;
}s[1001];

int cmp(const void*a,const void*b){
	struct In*c=(In*)a;
	struct In*d=(In*)b;
	if(c->b==d->b){
		return c->a-d->a;
	}
	else{
		return d->b-c->b;
	}
} 

int main(){
	int n;
	while(scanf("%d",&n)!=EOF){
		
		for(int i=0;i<n;i++){
			scanf("%d",&s[i].a);
			s[i].b=1;
		}
		for(int i=0;i<n-1;i++){
			if(s[i].b){
				for(int j=i+1;j<n;j++){
					if(s[i].a==s[j].a){
						s[i].b++;
						s[j].b=0;
					}
				}
			}
		}
		qsort(s,n,sizeof(s[0]),cmp);
		printf("%d\n",s[0].a);
	}
	return 0;
}
